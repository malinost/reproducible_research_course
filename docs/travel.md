# Finding the course venue

1. Transport yourself to Gothenburg (see e.g. [SJ](https://www.sj.se) or [MTR Express](https://www.mtrexpress.se)).
2. Go to Chalmers University of Technology ([Kemigården 4, 412 58 Göteborg](https://goo.gl/maps/WB1LcgKK9CN2)). The closest tram stops are Kapellplatsen or Chalmers (search for public transport options [here](https://www.vasttrafik.se/en/)).
3. Enter the Chemistry building through the main entrance. From there on, there will be signs pointing you in the right direction. We will be in a seminar room called 10:an.

# Finding the restaurant for the course dinner

We will have the course dinner at Market. It is located on Rosenlundsgatan 8. The closest tram stops are Hagakyrkan, Järntorget or Grönsakstorget.
