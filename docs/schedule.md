# Schedule

The course will be in "10:an", Chemistry building, Chalmers University of Technology, Gothenburg (see [travel info](travel.md)).

<table>
  <tr>
    <td colspan="3">
      <font size="4">
      <center> Day 1 - 2018-11-28 </center>
    </td>
  </tr>
  <tr>
    <td> <font size="3"><b>Time</b> </td>
    <td> <font size="3"><b>Topic</b> </td>
  </tr>
  <tr>
    <td> <font size="3"> 09:00
    <td> <font size="3"> Introduction to Reproducible Research [<a href="https://bitbucket.org/scilifelab-lts/rep_res_slides_1811/raw/master/introduction.pdf">slides</a>]
</td>
  </tr>
  <tr>
    <td> <font size="3"> 09:45 </td>
    <td> <font size="3"> Data management and project organization [<a href="https://bitbucket.org/scilifelab-lts/rep_res_slides_1811/raw/master/data_management.pdf">slides</a>]</td>
  </tr>
  <tr>
    <td> <font size="3"> 10:15 </td>
    <td> <font size="3"> Fika break </td>
  </tr>
  <tr>
    <td> <font size="3"> 10:45 </td>
    <td>
      <font size="3"> Master your dependencies - environments and reproducibility
      <font size="2"><i><br>
      - Introduction to the package and environment manager Conda<br>
      - Practical tutorial: <a href="../conda/">Conda</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 12:00  </td>
    <td> <font size="3"> Lunch </td>
  </tr>
  <tr>
    <td> <font size="3"> 13:00 </td>
    <td>
      <font size="3"> Organize your analysis using workflow managers
      <font size="2"><i><br>
      - Introduction to Snakemake<br>
      - Practical tutorial: <a href="../snakemake/">Snakemake</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 16:15 </td>
    <td> <font size="3"> Wrap-up </td>
  </tr>
  <tr>
    <td> <font size="3"> 16:30 </td>
    <td> <font size="3"> Free time!</td>
  </tr>
  <tr>
    <td> <font size="3"> 17:00 </td>
    <td> <font size="3"> Joint departure for dinner</td>
  </tr>
  <tr>
    <td> <font size="3"> 17:30 </td>
    <td> <font size="3"> Dinner at <a href="http://marketrosenlund.se">Market</a></td>
  </tr>
  <tr>
    <td colspan="3"> </td>
  </tr>
  <tr>
    <td colspan="3">
      <font size="4">
      <center> Day 2  - 2018-11-29 </center>
    </td>
  </tr>
  <tr>
    <td> <font size="3"><b>Time</b> </td>
    <td> <font size="3"><b>Topic</b> </td>
  </tr>
  <tr>
    <td> <font size="3"> 08:30 </td>
    <td>
      <font size="3"> Distributing and version tracking your code
      <font size="2"><i><br>
      - Introduction to version control and git<br>
      - Practical tutorial: <a href="../git/">Git</a>
      <br><br>
      <font size="3"> Computational notebooks
      <font size="2"><i><br>
      - Introduction to Jupyter<br>
      - Practical tutorial: <a href="../jupyter/">Jupyter</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 12:00
    <td> <font size="3"> Lunch </td>
  </tr>
  <tr>
    <td> <font size="3"> 13:00 </td>
    <td>
      <font size="3"> Reproducible reports
      <font size="2"><i><br>
      - Introduction to R Markdown<br>
      - Practical tutorial: <a href="../rmarkdown/">R Markdown</a>
      <br><br>
      <font size="3"> Containerization
      <font size="2"><i><br>
      - Introduction to containers<br>
      - Practical tutorial: <a href="../docker/">Docker</a>
    </td>
  </tr>
  <tr>
    <td> <font size="3"> 16:00 </td>
    <td> <font size="3"> Wrap-up</td>
  </tr>
  <tr>
    <td> <font size="3"> 16:30 </td>
    <td> <font size="3"> All done! </td>
  </tr>
</table>

**Teachers:**  
Rasmus Ågren  
Leif Wigge  
Viktor Jonsson  
