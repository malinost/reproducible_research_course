# About
The material available on these pages is produced and maintained for a course on how to make your data analyses reproducible. In particular, it covers:

* Data management
* Conda
* Snakemake
* Git
* Jupyter
* R Markdown
* Docker

# Material

Slides from lectures covering the topics above are available under [Schedule](schedule.md). If you're looking for slides/tutorials from a particular instance of the course, select the appropriate Read the Docs tag in the bottom of the panel to the left.

This documentation and all the resources used in the course are available as a [Bitbucket repo](https://bitbucket.org/scilifelab-lts/reproducible_research_course.git).

## The authors
Leif Wigge and Rasmus Ågren  
[SciLifeLab](https://www.scilifelab.se), [National Bioinformatics Infrastructure Sweden (NBIS)](https://www.nbis.se), Bioinformatics Long-term Support

## License
MIT (see `LICENCE.txt` in the [Bitbucket repo](https://bitbucket.org/scilifelab-lts/reproducible_research_course.git)).
